import React, { Component } from "react";
const apiUrl = 'http://localhost:8080/api/v1/user';
export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Statistics : [],
            tableView: '',
            fromDate:'',
            toDate:''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.clear = this.clear.bind(this);
        
    }

    handleSubmit(event) {
        event.preventDefault();
        if(!this.state.fromDate && !this.state.toDate){
            alert('please enter valid date range')
            return false;
        }
        this.getDataFromApi(apiUrl+`?start=${this.state.fromDate}&end=${this.state.toDate}`)
        
    }
    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }
    componentWillMount() {
        this.getDataFromApi(apiUrl)
    }
    getDataFromApi(url){
        fetch(url)
        .then((response) => response.json())
        .then((data) =>{
            if(data.data.length > 0){
                this.setState({Statistics:data.data},this.renderTableData)
            }else{
                this.setState({Statistics:[]})
            }
        });
    }
    
    renderTableData() {
        return this.state.Statistics.map((statData, index) => {
           const { websiteId,chats,missedChats } = statData //destructuring
           return (
              <tr key={index}>
                 <td>{websiteId}</td>
                 <td>{chats}</td>
                 <td>{missedChats}</td>
              </tr>
           )
        })
     }
    clear(){
        this.setState({fromDate:'', toDate:''},this.getDataFromApi(apiUrl))
    }
    render() {
        return (
            this.state.Statistics.length > 0 ?
                <>
                <form onSubmit={this.handleSubmit}>
                    
                    <input type="date" name="fromDate" defaultValue={this.state.fromDate} onChange={this.handleChange} /><label>From date</label>
                    
                    <input type="date" name="toDate" onChange={this.handleChange} defaultValue={this.state.toDate} /><label>To date</label>
                    <br/>
                    <button type="submit">Filter</button>
                    <button type="button" onClick={this.clear}>Clear</button>
                </form>
                <table>
                <thead>
                <tr>
                    <th>Website Id</th>
                    <th>Chats</th>
                    <th>Missed chats</th>
                </tr>
                </thead>
                <tbody>
                {this.renderTableData()}
                </tbody>
                </table>
                </>
            : 
            <h3>No data found</h3>
        );
    }
}